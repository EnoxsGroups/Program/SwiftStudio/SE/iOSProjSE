//
//  ViewController.swift
//  iOSProjSE
//
//  Created by Enoxs on 2019/7/6.
//  Copyright © 2019 Enoxs. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func showMessage(sender: UIButton){
        let alertController = UIAlertController(title: "Hello", message : "Hello , OwO / " , preferredStyle: UIAlertController.Style.alert)
        alertController.addAction(UIAlertAction(title: "OK",style: UIAlertAction.Style.default,handler: nil))
        present(alertController,animated: true,completion: nil)
    }
}

